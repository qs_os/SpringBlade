/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (821034742@qq.com).
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springblade.system.vo

import com.fasterxml.jackson.annotation.JsonInclude
import io.swagger.annotations.ApiModel
import lombok.Data
import lombok.EqualsAndHashCode
import org.springblade.core.tool.node.INode
import org.springblade.system.entity.Dept

/**
 * 视图实体类
 *
 * @author Chill
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "DeptVO对象", description = "DeptVO对象")
class DeptVO : Dept(), INode {
	override fun getParentId(): Int {
		return parentId!!
	}

	override fun getId(): Int {
		return id!!
	}

	/**
	 * 子孙节点
	 */
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private var children: List<INode>? = null

	/**
	 * 上级部门
	 */
	var parentName: String? = null
		set(parentName) {
			field = this.parentName
		}

	override fun getChildren(): List<INode> {
		return this.children ?: arrayListOf()
	}

	companion object {
		const val serialVersionUID = 1L
	}

}
