/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (821034742@qq.com).
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springblade.system.entity

import com.baomidou.mybatisplus.annotation.IdType
import com.baomidou.mybatisplus.annotation.TableId
import com.baomidou.mybatisplus.annotation.TableLogic
import com.baomidou.mybatisplus.annotation.TableName
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import java.io.Serializable

/**
 * 实体类
 *
 * @author Chill
 */
@TableName("blade_dept")
@ApiModel(value = "Dept对象", description = "Dept对象")
open class Dept : Serializable {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
      var id: Int? = null

    /**
     * 租户编号
     */
    @ApiModelProperty(value = "租户编号")
    var tenantCode: String? = null

    /**
     * 父主键
     */
    @ApiModelProperty(value = "父主键")
    var parentId: Int? = null

    /**
     * 部门名
     */
    @ApiModelProperty(value = "部门名")
    var deptName: String? = null

    /**
     * 部门全称
     */
    @ApiModelProperty(value = "部门全称")
    var fullName: String? = null

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    var sort: Int? = null

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    var remark: String? = null

    /**
     * 是否已删除
     */
    @TableLogic
    @ApiModelProperty(value = "是否已删除")
    var isDeleted: Int? = null

    companion object {
        private const val serialVersionUID = 1L
    }

}
